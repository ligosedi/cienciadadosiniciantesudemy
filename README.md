# CienciaDadosIniciantesUdemy



## Sobre esse projeto

Esse projeto foi criado para receber as submissões dos arquivos desenvolvidos durante o desenvolvimento do curso Ciência de Dados para iniciantes + Projetos 2023, realizado por mim, Alexandre.

O curso é ministrado pelo professor André Lacono na plataforma Udemy e pode ser encontrado integralmente durante o ano de 2023 através do seguinte link: [Link do Curso](https://www.udemy.com/course/python-data-science-para-iniciantes/)


Data de ínicio do Curso: 22/01/2023


## Conteúdos do curso:

[1] - Plataforma Anaconda (Realizado)  
[2] - Pandas (Realizado)  
[3] - Pandas Mod  
[4] - Numpy  
[5] - Matplot Lib  
[6] - Projeto 1 - Desenvolvido com o uso da Lib Pandas  
[7] - Projeto 2 - Desenvolvido com o uso da Lib Pandas e da Lib Matplotlib


## Tecnologias Utilizadas até o momento:

[1] - Plataforma Anaconda  
[2] - Matplotlib  
[3] - Numpy  
[4] - Pandas  
[5] - Gitlab  
[6] - JupyterLab 3.4.4  
[7] - VSCode 1.74.3